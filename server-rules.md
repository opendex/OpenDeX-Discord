# Server Rules

## Discord Server Constraints:

- It has 5 seconds of waiting time between sent messages (except silver, gold and diamond members)
- Must have a verified email, be registered on Discord for more than 5 minutes and be member of our server for more than 10 minutes

## Basic rules

1. Treat everyone with respect. Absolutely no harassment, put downs, bullying, witch hunting, sexism, racism, anti-LGBTQ+ speech, hate speech or degrading content will be tolerated.
2. No spam or self-promotion (server invites, advertisements, etc) without permission from a staff member. This includes DMing fellow members.
3. No adult (18+), explicit, or controversial messages.
4. No political or religious topics.
5. No flooding the chat with messages. Do not type in ALL CAPS.
6. No NSFW or obscene content. This includes text, images, or links featuring nudity, sex, hard violence, or other graphically disturbing content.
7. No excessive cursing.
8. No advertising  (Only with Permission).
9.  No referral links.
10. No begging or repeatedly asking for help in the chat. Repeatingly asking basic questions will lead to administrative action
11. No offensive names.
12. Sending/Linking any harmful material such as viruses, IP grabbers or harmware results in an immediate and permanent ban.
13. Use proper grammar and spelling and don't spam.
14. Mentioning @everyone / @here , the Moderators or a specific person without proper reason or doing mass mentioning is prohibited.
15. Post content in the correct channels.
16. Don't post someone's personal information without permission.
17. Listen to what Staff says. Do not argue with staff. Decisions are final.
18. Act civil.
19. Moderators and Admins reserve the right to delete and edit posts.
20. Moderators and Admins reserve the right to disconnect you from a voice channel if your sound quality is poor.
21. Moderators and Admins reserve the right to disconnect, mute, deafen, or move members to and from voice channels.
22. Inviting bots is NOT ALLOWED without administrative approval, any bots that are found will be INSTANTLY BANNED.
23. Do not perform or promote the intentional use of glitches, hacks, bugs, and other exploits that will cause an incident within the community and other players.
24. If you see something against the rules or something that makes you feel unsafe, let staff know. We want this server to be a welcoming space!
25. By using this servers you agree to these rules. Infringement of any of these rules will lead to immediate ban and legal action if needed.
26. No Scams. You will be immediately blocked and banned. You may also be subject to investigation.


## Voice Chat Rules

1. No voice chat surfing or switching channels repeatedly.
2. No annoying, loud or high pitch noises.
3. Reduce the amount of background noise, if possible. Resort to push to talk in your settings to reduce the issue.
4. Moderators and Admins reserve the right to disconnect you from a voice channel if your sound quality is poor.
5. Moderators and Admins reserve the right to disconnect, mute, deafen, or move members to and from voice channels.


## Consequences of Rule Breakage

1. The server uses bots for moderation which send a message to the concerned person if several emojis, repeated letters, multiple capitals, certain speech and links are used. A report is filed which can be then viewed by the Moderators for action.
2. Our bots document each and every edit or delete to messages, so no one can get away by modifying their offensive content after posting it.
3. More serious offences can involve message deletion, a warning or even direct expulsion from the server.
4. Each and every case will be subjected to the decision of the Moderators and Admins which will be final and binding.








